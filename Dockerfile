FROM daleglass/hifi-fedora-build
MAINTAINER Dale Glass <daleglass@gmail.com>

# Dockferfile for faster builds.
# Based on the parent, plus a source tree and the tools installed by vspkg

ARG repository=https://github.com/highfidelity/hifi
ARG sourcedir=/home/hifi/source

RUN echo Sourcedir is $sourcedir

RUN git clone ${repository} ${sourcedir}

RUN mkdir ${sourcedir}/build
WORKDIR ${sourcedir}/build

RUN cmake .. -DOpenGL_GL_PREFERENCE:STRING=GLVND
RUN rm -rf "$sourcedir"

ENTRYPOINT /bin/bash

